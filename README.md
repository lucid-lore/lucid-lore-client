# client

## Preparation
```
    cp src/config/index{.example,}.js
```

## Project setup
```
yarn install
```

### Compiles and hot-reloads for development
```
yarn run serve
```

### Compiles and minifies for production
```
yarn run build
```


## Icons
https://material.io/resources/icons/?style=baseline


## Code Generation
    
    npm i -g hygen
    
    hygen crudEntity new %someNameEntities%
