import Module from '../api/base/Module';
import { API_URL } from './index';


const api = Module(API_URL);


export default {
    login: api.post('/login', false),
    session: api.get('/session'),
    school: api.get('/school'),
    logout: api.post('/logout'),

    getActualTestSessions: api.get('/testApp/sessions'),
    getActiveTestSession: api.get('/testApp/active'),
    setActiveTestSession: api.post('/testApp/active/:id'),
    postTestAppAnswer: api.post('/testApp/answer'),
    dropActiveTestSession: api.drop('/testApp/active'),

    schoolsList: api.get('/admin/schools'),
    schoolsShortList: api.get('/admin/schools/short'),
    schoolsCreateItem: api.post('/admin/schools'),
    schoolsItem: api.get('/admin/schools/:id'),
    schoolsEditItem: api.put('/admin/schools/:id'),
    schoolsDropItem: api.drop('/admin/schools/:id'),

    classesList: api.get('/admin/classes'),
    classesShortList: api.get('/admin/classes/short'),
    classesCreateItem: api.post('/admin/classes'),
    classesItem: api.get('/admin/classes/:id'),
    classesEditItem: api.put('/admin/classes/:id'),
    classesDropItem: api.drop('/admin/classes/:id'),

    usersList: api.get('/admin/:roles'),
    usersShortList: api.get('/admin/users/short'),
    usersCreateItem: api.post('/admin/:roles'),
    usersItem: api.get('/admin/:roles/:id'),
    usersEditItem: api.put('/admin/:roles/:id'),
    usersDropItem: api.drop('/admin/:roles/:id'),

    disciplinesList: api.get('/admin/disciplines'),
    disciplinesShortList: api.get('/admin/disciplines/short'),
    disciplinesCreateItem: api.post('/admin/disciplines'),
    disciplinesItem: api.get('/admin/disciplines/:id'),
    disciplinesEditItem: api.put('/admin/disciplines/:id'),
    disciplinesDropItem: api.drop('/admin/disciplines/:id'),

    subjectsList: api.get('/admin/subjects'),
    subjectsShortList: api.get('/admin/subjects/short'),
    subjectsCreateItem: api.post('/admin/subjects'),
    subjectsItem: api.get('/admin/subjects/:id'),
    subjectsEditItem: api.put('/admin/subjects/:id'),
    subjectsMoveItemUp: api.put('/admin/subjects/:id/up'),
    subjectsMoveItemDown: api.put('/admin/subjects/:id/down'),
    subjectsDropItem: api.drop('/admin/subjects/:id'),

    testGroupsList: api.get('/admin/testGroups'),
    testGroupsShortList: api.get('/admin/testGroups/short'),

    getTestGroupUsersList: api.get('/admin/testGroupUsers/:id'),
    addUserToTestGroup: api.post('/admin/testGroupUsers/:id/:userId'),
    dropUserFromGroup: api.drop('/admin/testGroupUsers/:id/:userId'),

    testGroupsCreateItem: api.post('/admin/testGroups'),
    testGroupsItem: api.get('/admin/testGroups/:id'),
    testGroupsEditItem: api.put('/admin/testGroups/:id'),
    testGroupsDropItem: api.drop('/admin/testGroups/:id'),

    testsList: api.get('/admin/tests'),
    testsShortList: api.get('/admin/tests/short'),
    testsCreateItem: api.post('/admin/tests'),
    testsItem: api.get('/admin/tests/:id'),
    testsEditItem: api.put('/admin/tests/:id'),
    testsDropItem: api.drop('/admin/tests/:id'),

    createTestQuestion: api.post('/admin/tests/:testId/questions'),
    getTestQuestions: api.get('/admin/tests/:testId/questions'),
    saveTestQuestions: api.put('/admin/tests/questions/:id'),
    moveTestQuestionUp: api.put('/admin/tests/questions/:id/up'),
    moveTestQuestionDown: api.put('/admin/tests/questions/:id/down'),
    dropTestQuestion: api.drop('/admin/tests/questions/:id'),

    testSessionsList: api.get('/admin/testSessions'),
    testSessionsShortList: api.get('/admin/testSessions/short'),
    testSessionsCreateItem: api.post('/admin/testSessions'),
    testSessionsItem: api.get('/admin/testSessions/:id'),
    testSessionsEditItem: api.put('/admin/testSessions/:id'),
    testSessionsDropItem: api.drop('/admin/testSessions/:id'),

    testSessionsItemInfo: api.get('/admin/testSessions/:id/info'),
    ///testSessionsItemStatus: api.get('/admin/testSessions/:id/status'),
    setTestSessionStatus: api.put('/admin/testSessions/:id/status'),

    /// HERE Inject Resources ///
};
