import io from 'socket.io-client';
import { APP_URL } from '../config/index';
import sessionService from './session';
import { syncWithServerTime } from '../utils/datetime';


function connectToSocket() {
    const socket = io.connect(APP_URL);

    socket.on('getToken', () => {
        socket.emit('token', sessionService.getToken());
    });

    socket.on('now', (d) => {
        syncWithServerTime( new Date(d) );
    });


    return {
        on: (...args) => socket.on(...args),
        off: (...args) => socket.off(...args),
        emit: (...args) => socket.emit(...args),
        join: channel => socket.emit('join', channel),
        leave: channel => socket.emit('leave', channel),
        disconnect: () => socket.disconnect(),
    };
}


export default connectToSocket;