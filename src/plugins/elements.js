import Vue from 'vue';

import wCombobox from '../components/elements/wCombobox';
import UserSchoolInput from '../components/elements/inputs/UserSchool';

Vue.component('w-combobox', wCombobox);
Vue.component('user-school-input', UserSchoolInput);
