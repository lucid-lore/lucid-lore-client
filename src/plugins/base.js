import Vue from 'vue';
import { mapGetters } from 'vuex';
import ACTIONS from '../constants/actions';
import { humanDatetimeFormat, formatSeconds } from '../utils/datetime';


Vue.mixin({
    data: () => ({
        ACTIONS
    }),

    computed: mapGetters([ 'checkPermission' ]),

    methods: {
        humanDatetimeFormat,
        formatSeconds
    }
});
