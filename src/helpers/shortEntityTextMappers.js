
const schools = ({ id, name }) => `${id}. ${name}`;

const disciplines = ({ id, name }) => `${id}. ${name}`;

const subjects = ({ id, name }) => `${id}. ${name}`;

const classes = ({ id, level, letter, name }) =>
    `${id}. ${level || ''}${letter || ''} ${name || ''}`;

const users = ({ id, login, fio }) => `${id}. ${fio || login}`;

const tests = ({ id, name }) => `${id}. ${name}`;

const testGroups = ({ id, name }) => `${id}. ${name}`;


export default {
    schools,
    disciplines,
    subjects,
    classes,
    users,
    tests,
    testGroups,
};
