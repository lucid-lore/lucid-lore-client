import { format, addMinutes, differenceInSeconds, addSeconds } from 'date-fns'


let timeDelta = 0;
export const syncWithServerTime = (serverDate) => {
    timeDelta = differenceInSeconds(new Date, serverDate);
    console.log('____timeDelta', timeDelta);
};




export const HUMAN_DATETIME_FORMAT = 'dd.MM.yyyy HH:mm:ss';

export const humanDatetimeFormat = (isoText) => {
    if (!isoText) return '';

    return format(new Date(isoText), HUMAN_DATETIME_FORMAT);
};


export const getRemainingSeconds = (before) => {
    const beforeSynced = addSeconds(new Date(before), timeDelta);

    return differenceInSeconds(beforeSynced, new Date);
};

export const getPassedSeconds = (after) => {
    const afterSynced = addSeconds(new Date(after), timeDelta);

    return differenceInSeconds(new Date, afterSynced);
};


export const formatSeconds = (seconds) => {
    if (seconds < 0) return '';

    const mins = Math.floor(seconds / 60);
    const secs = seconds % 60;

    return `${mins}:${secs < 10 ? '0' : ''}${secs}`;
};


