
export const TEST_SESSIONS_STATUSES_MAP = {
    draft: 'Черновик',
    planned: 'План',
    preparing: 'Подготовка',
    active: 'Активна',
    finished: 'Завершена',
};
