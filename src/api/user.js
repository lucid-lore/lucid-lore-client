import makeRequest from './base';


const doLogin = (data) => makeRequest('login', data);

const getSession = () => makeRequest('session');

const getSchool = () => makeRequest('school');

const doLogout = () => makeRequest('logout');


export default {
    doLogin,
    getSession,
    getSchool,
    doLogout,
};
