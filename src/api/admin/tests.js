import makeRequest from '../base';


const getList = (params) => makeRequest({ res: 'testsList', params });

const getShortList = (params) => makeRequest({ res: 'testsShortList', params });

const createItem = data => makeRequest({ res: 'testsCreateItem' }, data);

const getItem = id => makeRequest({ res: 'testsItem', id });

const editItem = (id, data) => makeRequest({ res: 'testsEditItem', id }, data);

const dropItem = id => makeRequest({ res: 'testsDropItem', id });

const createTestQuestion = testId => makeRequest({ res: 'createTestQuestion', testId });

const getTestQuestions = testId => makeRequest({ res: 'getTestQuestions', testId });

const saveTestQuestion = (id, data) => makeRequest({ res: 'saveTestQuestions', id }, data);

const moveTestQuestionUp = (id) => makeRequest({ res: 'moveTestQuestionUp', id });

const moveTestQuestionDown = (id) => makeRequest({ res: 'moveTestQuestionDown', id });

const dropTestQuestion = (id) => makeRequest({ res: 'dropTestQuestion', id });


export default {
    getList,
    getShortList,
    createItem,
    getItem,
    editItem,
    dropItem,

    createTestQuestion,
    getTestQuestions,
    saveTestQuestion,
    moveTestQuestionUp,
    moveTestQuestionDown,
    dropTestQuestion,
};

