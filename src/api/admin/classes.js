import makeRequest from '../base';


const getList = (params) => makeRequest({ res: 'classesList', params });

const getShortList = (params) => makeRequest({ res: 'classesShortList', params });

const createItem = data => makeRequest({ res: 'classesCreateItem' }, data);

const getItem = id => makeRequest({ res: 'classesItem', id });

const editItem = (id, data) => makeRequest({ res: 'classesEditItem', id }, data);

const dropItem = id => makeRequest({ res: 'classesDropItem', id });


export default {
    getList,
    getShortList,
    createItem,
    getItem,
    editItem,
    dropItem,
};
