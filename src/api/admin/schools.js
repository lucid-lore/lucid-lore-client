import makeRequest from '../base';


const getList = (params) => makeRequest({ res: 'schoolsList', params });

const getShortList = (params) => makeRequest({ res: 'schoolsShortList', params });

const createItem = data => makeRequest({ res: 'schoolsCreateItem' }, data);

const getItem = id => makeRequest({ res: 'schoolsItem', id });

const editItem = (id, data) => makeRequest({ res: 'schoolsEditItem', id }, data);

const dropItem = id => makeRequest({ res: 'schoolsDropItem', id });


export default {
    getList,
    getShortList,
    createItem,
    getItem,
    editItem,
    dropItem,
};
