import makeRequest from '../base';


const getList = (params) => makeRequest({ res: 'testGroupsList', params });

const getShortList = (params) => makeRequest({ res: 'testGroupsShortList', params });

const getTestGroupUsersList = (id) => makeRequest({ res: 'getTestGroupUsersList', id });

const addUserToTestGroup = (id, userId) => makeRequest({ res: 'addUserToTestGroup', id, userId });

const dropUserFromGroup = (id, userId) => makeRequest({ res: 'dropUserFromGroup', id, userId });


const createItem = data => makeRequest({ res: 'testGroupsCreateItem' }, data);

const getItem = id => makeRequest({ res: 'testGroupsItem', id });

const editItem = (id, data) => makeRequest({ res: 'testGroupsEditItem', id }, data);

const dropItem = id => makeRequest({ res: 'testGroupsDropItem', id });


export default {
    getList,
    getShortList,
    getTestGroupUsersList,
    addUserToTestGroup,
    dropUserFromGroup,

    createItem,
    getItem,
    editItem,
    dropItem,
};

