import makeRequest from '../base';


const getList = (params) => makeRequest({ res: 'subjectsList', params });

const getShortList = (params) => makeRequest({ res: 'subjectsShortList', params });

const createItem = data => makeRequest({ res: 'subjectsCreateItem' }, data);

const getItem = id => makeRequest({ res: 'subjectsItem', id });

const editItem = (id, data) => makeRequest({ res: 'subjectsEditItem', id }, data);

const moveItemUp = (id) => makeRequest({ res: 'subjectsMoveItemUp', id });

const moveItemDown = (id) => makeRequest({ res: 'subjectsMoveItemDown', id });

const dropItem = id => makeRequest({ res: 'subjectsDropItem', id });


export default {
    getList,
    getShortList,
    createItem,
    getItem,
    editItem,
    moveItemUp,
    moveItemDown,
    dropItem,
};

