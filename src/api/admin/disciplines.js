import makeRequest from '../base';


const getList = (params) => makeRequest({ res: 'disciplinesList', params });

const getShortList = (params) => makeRequest({ res: 'disciplinesShortList', params });

const createItem = data => makeRequest({ res: 'disciplinesCreateItem' }, data);

const getItem = id => makeRequest({ res: 'disciplinesItem', id });

const editItem = (id, data) => makeRequest({ res: 'disciplinesEditItem', id }, data);

const dropItem = id => makeRequest({ res: 'disciplinesDropItem', id });


export default {
    getList,
    getShortList,
    createItem,
    getItem,
    editItem,
    dropItem,
};
