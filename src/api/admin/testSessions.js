import makeRequest from '../base';


const getList = (params) => makeRequest({ res: 'testSessionsList', params });

const getShortList = (params) => makeRequest({ res: 'testSessionsShortList', params });

const createItem = data => makeRequest({ res: 'testSessionsCreateItem' }, data);

const getItem = id => makeRequest({ res: 'testSessionsItem', id });

const editItem = (id, data) => makeRequest({ res: 'testSessionsEditItem', id }, data);

const dropItem = id => makeRequest({ res: 'testSessionsDropItem', id });

const getInfo = id => makeRequest({ res: 'testSessionsItemInfo', id });

const setStatus = (id, status) => makeRequest({ res: 'setTestSessionStatus', id }, { status });


export default {
    getList,
    getShortList,
    createItem,
    getItem,
    editItem,
    dropItem,

    getInfo,
    setStatus,
};

