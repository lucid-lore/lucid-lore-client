import makeRequest from './base';


const getActualTestSessions = () => makeRequest('getActualTestSessions');

const getActiveTestSession = () => makeRequest('getActiveTestSession');

const setActiveTestSession = (id) => makeRequest({ res: 'setActiveTestSession', id });

const postTestAppAnswer = (data) => makeRequest('postTestAppAnswer', data);

const dropActiveTestSession = () => makeRequest({ res: 'dropActiveTestSession' });


export default {
    getActualTestSessions,
    getActiveTestSession,
    setActiveTestSession,
    dropActiveTestSession,
    postTestAppAnswer,
};
