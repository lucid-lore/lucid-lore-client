import Vue from 'vue';
import VueRouter from 'vue-router';
import sessionService from './services/session';
import logger from './services/logger';
import Store from './store';
import ACTIONS from './constants/actions';

import Login from './views/Login';
import Dashboard from './views/Dashboard';
import TestApp from './views/pupil/TestApp';
import AdminSchools from './views/admin/Schools';
import AdminClasses from './views/admin/Classes';
import AdminUsersLayout from './views/admin/UsersLayout';
import AdminUsersPage from './views/admin/UsersPage';
import AdminDisciplines from './views/admin/Disciplines';
import AdminSubjects from './views/admin/Subjects';
import AdminTestGroups from './views/admin/TestGroups';
import AdminTestGroup from './views/admin/TestGroup';
import AdminTests from './views/admin/Tests';
import AdminTest from './views/admin/Test';
import AdminTestSessions from './views/admin/TestSessions';
import AdminTestSession from './views/admin/TestSession';
/// HERE Inject View Components Import ///


Vue.use(VueRouter);

const auth = true;
const nonAuth = true;
const headerBar = true;
const navigation = true;


const routes = [
    {
        path: '/login',
        name: 'login',
        component: Login,
        meta: { nonAuth }
    },

    {
        path: '/authed',
        redirect: to => {
            const user = Store.getters.user;
            if (!user.role) return '/login';

            return DEFAULT_ROUTES_BY_ROLES[user.role];
        }
    },

    {
        path: '/dashboard',
        name: 'dashboard',
        component: Dashboard,
        meta: { auth, navigation, headerBar, permission: ACTIONS.VIEW_DASHBOARD, }
    },
    {
        path: '/testApp',
        name: 'testApp',
        component: TestApp,
        meta: { auth, headerBar }
    },

    {
        path: '/admin/users',
        redirect: '/admin/users/pupils',
        component: AdminUsersLayout,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, },
        children: [
            {
                path: 'admins',
                component: AdminUsersPage,
                meta: { auth, headerBar, navigation, usersPageRole: 'admin', permission: ACTIONS.VIEW_ADMIN, }
            },
            {
                path: 'school_admins',
                component: AdminUsersPage,
                meta: { auth, headerBar, navigation, usersPageRole: 'school_admin', permission: ACTIONS.VIEW_ADMIN, }
            },
            {
                path: 'teachers',
                component: AdminUsersPage,
                meta: { auth, headerBar, navigation, usersPageRole: 'teacher', permission: ACTIONS.VIEW_ADMIN, }
            },
            {
                path: 'pupils',
                component: AdminUsersPage,
                meta: { auth, headerBar, navigation, usersPageRole: 'pupil', permission: ACTIONS.VIEW_ADMIN, }
            },
        ]
    },
    {
        path: '/admin/classes',
        component: AdminClasses,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, }
    },
    {
        path: '/admin/schools',
        component: AdminSchools,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, }
    },
    {
        path: '/admin/disciplines',
        component: AdminDisciplines,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, }
    },

    {
        path: '/admin/subjects',
        component: AdminSubjects,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, }
    },

    {
        path: '/admin/testGroups',
        component: AdminTestGroups,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, }
    },
    {
        path: '/admin/testGroups/:id',
        component: AdminTestGroup,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, },
        props: true,
    },

    {
        path: '/admin/tests',
        component: AdminTests,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, }
    },
    {
        path: '/admin/tests/:id',
        component: AdminTest,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, },
        props: true,
    },
    {
        path: '/admin/testSessions',
        component: AdminTestSessions,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, }
    },
    {
        path: '/admin/testSessions/:id',
        component: AdminTestSession,
        meta: { auth, headerBar, navigation, permission: ACTIONS.VIEW_ADMIN, },
        props: true,
    },
    /// HERE Inject Routes ///

    /*{
      path: '/about',
      name: 'about',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ ///'../views/About.vue')
    //}*/
];


const router = new VueRouter({
    mode: 'history',
    routes
});
export default router;


export const DEFAULT_ROUTES_BY_ROLES = {
    admin: '/dashboard',
    school_admin: '/dashboard',
    teacher: '/dashboard',
    pupil: '/testApp',
};

/// CHECK AUTH & SIDE ///
router.beforeEach((to, from, next) => {
    const handler = () => {
        const user = Store.getters.user;
        const userPermissions = Store.getters.permissions;

        if (!to.matched.length) {
            logger.log('Miss route for', to.path);

            if (!from.matched.length) {
                logger.log('Init with unknown route');

                if (user && user.role) {
                    return next(DEFAULT_ROUTES_BY_ROLES[user.role]);
                }

                return next('/login');
            }

            return next(false);
        }

        if (to.meta.auth && !sessionService.isAuth()) {
            logger.log('Deny auth route', to.path);
            return next('/login');
        }

        if (to.meta.permission && !userPermissions.includes(to.meta.permission)) {
            logger.log('Deny route cause of permission', to.path);
            return next(DEFAULT_ROUTES_BY_ROLES[user.role]);
        }

        if (to.meta.nonAuth && sessionService.isAuth()) {
            logger.log('Deny nonAuth route', to.path);
            return next(DEFAULT_ROUTES_BY_ROLES[user.role]);
        }

        next();
    };

    if (sessionService.isAuthing()) {
        sessionService.addAfterAuthJob(handler);
    } else {
        handler();
    }
});
