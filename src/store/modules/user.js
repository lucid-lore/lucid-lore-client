import userApi from '../../api/user';
import {
    LOGIN_SUCCESS,
    LOGOUT_SUCCESS,
    USER_DATA,
    USER_PERMISSIONS,
    SCHOOL_DATA,
} from '../mutation-types';
import sessionService from '../../services/session';
import router from '../../router'



const state = {
    auth: false,
    user: {},
    permissions: [],
    school: null,
};


const getters = {
    auth: state => state.auth,
    user: state => state.user,
    school: state => state.school,
    permissions: state => state.permissions,

    checkPermission: state => name => state.permissions.includes(name)
};


const actions = {
    async login({ commit, dispatch }, data) {
        try {
            const response = await userApi.doLogin(data);
            commit(LOGIN_SUCCESS, response.data.sid);
            commit(USER_DATA, response.data.user);                              /// FIXME nesting data??
            commit(USER_PERMISSIONS, response.data.permissions);

            dispatch('school');

            return response.data.user;
        } catch (err) {
            console.log('LOGIN err', err);  /// FIXME maybe useless
            throw err;
        }
    },

    async session({ commit, dispatch }) {
        try {
            const response = await userApi.getSession();
            commit(LOGIN_SUCCESS);
            commit(USER_DATA, response.data.user);                              /// FIXME nesting data??
            commit(USER_PERMISSIONS, response.data.permissions);

            dispatch('school');
        } catch {
            commit(LOGOUT_SUCCESS);
        }

    },

    async school({ commit }) {
        const response = await userApi.getSchool();
        commit(SCHOOL_DATA, response.data);
    },

    async logout({ commit }) {
        await userApi.doLogout();
        commit(LOGOUT_SUCCESS);
        router.push('/login');
    }
};


const mutations = {
    [LOGIN_SUCCESS] (state, sid) {
        sid && sessionService.setToken(sid);
        state.auth = true;
    },

    [LOGOUT_SUCCESS] (state) {
        sessionService.dropToken();
        state.auth = false;
        state.user = {};
        state.permissions = [];
        state.school = null;
    },

    [USER_DATA] (state, user) {
        state.user = user;
    },

    [SCHOOL_DATA] (state, school) {
        state.school = school;
    },

    [USER_PERMISSIONS] (state, permissions) {
        state.permissions = permissions;
    },
};


export default {
    state,
    getters,
    actions,
    mutations
};
