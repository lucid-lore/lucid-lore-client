import testAppApi from '../../api/testApp';
import {
    SET_ACTIVE_TEST_SESSION_STATUS,
} from '../mutation-types';


const state = {
    active: null,
    activeLoaded: false,
};


const getters = {
    activeTestSessionStatus: state => state.active,
    activeTestSessionStatusLoaded: state => state.activeLoaded,
    testSession: state => state.active && state.active.testSession,
    questions: ({ active }) => active && active.testQuestions,
    activeQuestion: ({ active }) => active && active.testQuestions.find(q => q.order === active.questionNumber),
};


const actions = {
    async getActiveTestSessionStatus ({ commit }) {
        const response = await testAppApi.getActiveTestSession();

        commit(SET_ACTIVE_TEST_SESSION_STATUS, response.data);
    },

    async setActiveTestSession ({ dispatch }, id) {
        await testAppApi.setActiveTestSession(id);

        dispatch('getActiveTestSessionStatus');
    },

    async dropActiveTestSession ({ dispatch }) {
        await testAppApi.dropActiveTestSession();

        dispatch('getActiveTestSessionStatus');
    },

    async postAnswer ({ dispatch }, data) {
        await testAppApi.postTestAppAnswer(data);

        dispatch('getActiveTestSessionStatus');
    }
};


const mutations = {
    [SET_ACTIVE_TEST_SESSION_STATUS] (state, data) {
        state.active = data;
        state.activeLoaded = true;
    },
};


export default {
    state,
    getters,
    actions,
    mutations
};
